TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG += qt
CONFIG += c++11

SOURCES += main.cpp
INCLUDEPATH += "/usr/include/eigen3" \
                           "/usr/local/include/eigen3" \
               "/usr/local/include/pcl-1.7/" \
               "/usr/local/include/vtk-6.2/"

LIBS += -lboost_system\
        -lboost_thread\
        -lboost_timer\
        `pkg-config opencv --libs`\
        -lpcl_registration -lpcl_sample_consensus -lpcl_features -lpcl_filters -lpcl_surface -lpcl_segmentation \
        -lpcl_search -lpcl_kdtree -lpcl_octree -lflann_cpp -lpcl_common -lpcl_io \
        -lpcl_visualization \
        -lSDL -lSDLmain \
        -lX11 \
        -fopenmp
